import socket
import _thread

from tor_utils import create_circuit, connect_to_server, send_data_to_server, destroy_circuit
from get_nodes import get_nodes
import os
import sys
import time

from gui import get_node_num_gui

current_dir = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current_dir)
sys.path.append(parent)

from config import PROXY_LISTEN_HOST , PROXY_LISTEN_PORT, DIRECTORY_ADDRESS, SERVER_ADDRESS, NODES

LISTEN_HOST = PROXY_LISTEN_HOST
LISTEN_PORT = PROXY_LISTEN_PORT


def establish_circuit():
    """
    Creating a new circuit from nodes received from the directory node
    input: none
    output: the relay list for the circuit, connection to the entry node
    """
    relay_list = get_nodes(DIRECTORY_ADDRESS[0], DIRECTORY_ADDRESS[1], NODES)
    if not len(relay_list):  # got 0 nodes
        raise('no nodes found')
    conn_to_nodes = create_circuit(relay_list)
    connect_to_server(conn_to_nodes, relay_list, SERVER_ADDRESS)
    print("new circuit created - ", relay_list)
    return relay_list, conn_to_nodes

def handle_client(client_connection):
    """
    Handle client and server connection 
    send and receive data
    and reconnect if lost connection 
    """
    try:
        # Building a new circuit
        relay_list, conn_to_nodes = establish_circuit()
    except Exception as e:
        print(e)
        return
    msg = client_connection.recv(4096)
    if len(msg) == 0:
        return
    
    print('request received: ', msg)

    try:
        res = send_data_to_server(conn_to_nodes, relay_list, msg)
        print('received from server: ', res)
        client_connection.sendall(res)
    except:
        print('connection to nodes lost, rebuilding circuit')
        try:    # trying to reconnect when problem accures
            relay_list, conn_to_nodes = establish_circuit()
            client_connection.sendall(send_data_to_server(conn_to_nodes, relay_list, msg))
        except Exception as e:
            print(e)
            client_connection.sendall(b'Error')
            client_connection.close()
            return

    try:
        destroy_circuit(conn_to_nodes, relay_list[0])
    except:
        print('could not destroy circuit')

    client_connection.close()


def main():
    node_num = get_node_num_gui()   # gui app
    if node_num != False:
        NODES = node_num
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((LISTEN_HOST, LISTEN_PORT)), print("bind ")
            s.listen(), print("listen ")

            while True:
                conn, addr = s.accept()
                print ("new client ", addr)
                _thread.start_new_thread(handle_client, (conn,))


if __name__ == '__main__':
    main()

